#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

int BUFFLEN;
int ind;
int consumir;
int sync_b = -1;


typedef struct mnjObj{
	int *buff;
	int front;
	int back;
	int elementos;
	sem_t items;
	sem_t espacios;
	sem_t mutex;
}paquete;

typedef struct hiloObj{
	paquete *pqt;
	int indice;
}hilo;

void* promElementos(void *obj){
	int sum=0,datos=0;
	float *prom=(float*)malloc(sizeof(float));
	paquete* arg = (paquete*)obj;
	while (consumir>0){
		sum = sum + arg->elementos;
		datos++;
		usleep(3);
	}*prom = sum/datos;
	printf("sum %d - datos %d - prom %.2f\n",sum,datos,*prom);
	pthread_exit(prom);
}

void* threadPromedioTiempo(void *obj){
    paquete* arg = ((hilo*)obj)->pqt;
    int indice = -1;
    int tiempoSegundos = 0;
    while(consumir>0){
        if(arg->elementos >0){
            if(indice == -1){
                if(arg->front == BUFFLEN){
                    indice = 0 ;
                }else{
                    indice = arg->front;
                }
                
            }
            if(arg->back >= indice){
                printf("El tiempo de espera promedio es :%d seg",tiempoSegundos);
                tiempoSegundos=0;
                indice = -1;
                
            }else{
                tiempoSegundos++;
                usleep(1);
            }
        }
    }return NULL;
}

void* threadConsumidor(void *obj){
	paquete* arg = ((hilo*)obj)->pqt;
	while(consumir>0){
		//printf("%d\n",sync_b);
		if(sync_b == 0){
			
			sem_wait(&(arg->items));
			sem_wait(&(arg->mutex));
		}
		arg->buff[arg->back] = 0;
		arg->elementos--;
		arg->back++;
		if(arg->back == BUFFLEN) arg->back = 0;
		printf("Hilo %d consumio un item. Hay %d elementos en cola.\n",((hilo*)obj)->indice,arg->elementos);

		if(sync_b == 0){
			sem_post(&(arg->mutex));
			sem_post(&(arg->espacios));
		}
	
		usleep(3);
	}
	pthread_exit(NULL);
}

void* threadProductor(void *obj){
	paquete* arg = ((hilo*)obj)->pqt;
	while(consumir>0){
		int azar = rand() % BUFFLEN;
		if(sync_b == 0){
			sem_wait(&(arg->espacios));
			sem_wait(&(arg->mutex));
		}
		arg->buff[arg->front] = azar;
		arg->elementos++;
		arg->front++;
		if(arg->front == BUFFLEN) arg->front = 0;
		printf("Hilo %d produjo un item. Hay %d elementos en cola.\n",((hilo*)obj)->indice,arg->elementos);
		consumir--;
		if(sync_b == 0){
			sem_post(&(arg->mutex));
			sem_post(&(arg->items));
		}
	
		usleep(1);
	}
	pthread_exit(NULL);
}

int main(int argc, char *argv[]){
	if(argc < 9) abort();
	printf("Elementos : %d\n",argc);
//1 3 5 7
	int n,p,c,e,hilos_prod,hilos_cons,completo = 0;
	for(int i = 1;i<9;i=i+2){
		//printf("i = %d\n",i);
		n = strcmp(argv[i],"-n");
		p = strcmp(argv[i],"-p");
		c = strcmp(argv[i],"-c");
		e = strcmp(argv[i],"-e");
		if(n == 0){
			completo++;
			BUFFLEN = atoi(argv[i+1]);
			//exit(-1);
		}else if(p == 0){
			completo++;
			hilos_prod = atoi(argv[i+1]);
			//exit(-1);
		}else if(c == 0){
			completo++;
			hilos_cons = atoi(argv[i+1]);
			//exit(-1);
		}else if(e == 0){
			completo++;
			consumir = atoi(argv[i+1]);
			//exit(-1);
		}
	}if(completo<4){
		printf("debe ingresar las banderas -n -p -c -e con su respectivo numero ...\n");
		exit(-1);
	}if(argc == 10){
		sync_b = strcmp(argv[9],"sync");
		if(sync_b == 0){
			printf("Se sincronizan los hilos\n");
		}else{
			printf("No se sincronizan los hilos\n");
		}
	}else{
		printf("No se sincronizan los hilos\n");
	}
	printf("prod:%d - consu:%d - buff:%d - consum:%d\n",hilos_prod,hilos_cons,BUFFLEN,consumir);
	
	int buffer[BUFFLEN];

	pthread_t l_tid[hilos_prod+hilos_cons];
			
	paquete *pqt;
	pqt = (paquete*)malloc(sizeof(paquete));

	pqt->buff = buffer;
	pqt->front = 0;
	pqt->back = 0;
	sem_init(&(pqt->items),0,0);
	sem_init(&(pqt->espacios),0,BUFFLEN);
	sem_init(&(pqt->mutex),0,1);
	pqt->elementos = 0;
	int rc;
	int sobrante1 = hilos_cons;
	int sobrante2 = hilos_prod;
	pthread_t prom;
	rc = pthread_create(&prom,NULL,promElementos,(void*)pqt);
	if(rc){
		printf("ERROR\n");
		exit(-1);
	}
	
	for(int i = 0;i<(hilos_prod+hilos_cons);i++){
		if(sobrante2>0) {
			hilo* hp = (hilo*)malloc(sizeof(hilo));
			hp->pqt = pqt;
			hp->indice = i;
			rc = pthread_create(&(l_tid[i]),NULL,threadProductor,(void*)hp);
			//printf("cons - %d\n",sobrante1);
			if(rc){
				printf("ERROR\n");
				exit(-1);
			}sobrante2--;
		}else if(sobrante1>0){
			hilo* hc = (hilo*)malloc(sizeof(hilo));
			hc->pqt = pqt;
			hc->indice = i;
			rc = pthread_create(&(l_tid[i]),NULL,threadConsumidor,(void*)hc);
			//printf("prod - %d\n",sobrante2);
			if(rc){
				printf("ERROR\n");
				exit(-1);
			}sobrante1--;
		}		
		
	}
	
    
	//pthread_t tiempo;
	//pthread_create(&(tiempo),NULL,threadPromedioTiempo,(void*)pqt);
	//for(int i =0 ; i<BUFFLEN;i++){
	//	pthread_join(l_tid[i],NULL);
	//}
	float *f;
	pthread_join(prom,(void **)&f);
	printf("promedio de elementos en el buffer %.2f\n",*f);
	for (int i = 0;i<BUFFLEN;i++){
		printf("%d , ",buffer[i]);
	}printf("\n");
	free(pqt);
	free(f);
}
